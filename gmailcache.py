"""
gmail cache, using pymongo.
"""

import json

from pymongo import MongoClient
client = MongoClient()

db = client['gmailcache']

# TODO: fn to clean cache database

def get_message(service, id):
    """
    TODO: Use cache
    """
    msg = db.message.find_one({"_id": id})
    if not msg:
        msg = service.users().messages().get(userId='me', id=id, format='metadata').execute()
        msg["_id"] = msg["id"]
        del msg["id"]
        print("Inserting...")
        db.message.insert_one(msg)
#        msg2 = db.message.find_one({"_id": id})
#        assert msg == msg2
    return msg
