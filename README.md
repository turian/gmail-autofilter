# gmail-autofilter

Help build gmail filters automatically.


## Local setup

Make sure:
- you are using Python 3.7, not Python 3.6 or Python 3.8.

Requires [Pipenv](https://docs.pipenv.org/en/latest/).

On OSX:

```
brew install pipenv
```

Intall Mongo:

```
brew tap mongodb/brew
brew install mongodb-community
sudo brew services start mongodb-community
```

Run:

```
pipenv install
pipenv shell
```

Enable the [Gmail
API](https://developers.google.com/gmail/api/quickstart/python) and
save `credentials.json` into the working directory.

## TODO

* Submit verification request for app.
